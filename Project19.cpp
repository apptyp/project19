﻿
#include <iostream>
using namespace std;

class Animal
{
public:
    Animal() {}
    virtual void Voice() = 0;

};


class Dog : public Animal
{
public:
    Dog() {}
    void Voice() override
    {
        cout << "Gav gav" << endl;
    }
};

class Cat : public Animal
{
public:
    Cat() {}
    void Voice() override
    {
        cout << "Myau myau" << endl;
    }
};

class Parrot : public Animal
{
public:
    Parrot() {}
    void Voice() override
    {
        cout << "Chik chirik" << endl;
    }
};


int main()
{
    const int size = 3;

    Animal* animal[size]{new Dog, new Cat, new Parrot};

    for (int i = 0; i < size; i++)
    {
        animal[i]->Voice();
    }
    
}

